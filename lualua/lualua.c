#include <lua.h>
#include <lauxlib.h>

#include <alloca.h>
#include <assert.h>
#include <errno.h>
#include <fenv.h>
#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>

#include "luabox.h"

// That might be __declspec(dllexport) in MSVC
#define PUBLIC __attribute__((visibility("default")))

#define warn(message) puts("WARNING: " message)

void timeout_handler(int signal, siginfo_t *info, void *context)
{
	(void) context;
	assert(signal == SIGALRM);
	assert(info->si_signo == SIGALRM);
	assert(info->si_code == SI_TIMER);
	jmp_buf *homeway = info->si_value.sival_ptr;
	siglongjmp(*homeway, 1);
}

inline static struct timespec split_time(double time)
{
	struct timespec result;
	result.tv_sec = (time_t) time;
	result.tv_nsec = (long) (1e9 * (time - result.tv_sec));
	return result;
}

int lualua_runv(double timeout, struct string_list *code, buffer_ref memory, struct string_list **result)
{
	sigjmp_buf homeway;

	if (!result)
		return EFAULT;

	fenv_t fenv;
	feholdexcept(&fenv); // It can’t fail in musl. FIXME: What’s about glibc?

	timer_t timer;
	struct sigevent sev = {
		.sigev_notify = SIGEV_THREAD_ID,
		.sigev_signo = SIGALRM,
		.sigev_value.sival_ptr = &homeway,
		// .sigev_notify_thread_id = gettid(), // doesn’t work, there are no such symbols
		._sigev_un._tid = syscall(SYS_gettid),
	};
	if (timer_create(CLOCK_THREAD_CPUTIME_ID, &sev, &timer) != 0) // It *can* fail—in case of OOM
		goto exit;

	struct sigaction sa = {
		.sa_flags = SA_SIGINFO,
		.sa_sigaction = timeout_handler,
	};
	sigemptyset(&sa.sa_mask);
	struct sigaction old_sa;
	if (sigaction(SIGALRM, &sa, &old_sa) != 0) // It can’t fail unless something is terribly broken already
		goto exit_wtimer;

	// Starting the trip!
	int timed_out = sigsetjmp(homeway, 1);
	if (timed_out) {
		// the timer is not armed anymore, so it’s safe
		errno = ETIMEDOUT;
		goto exit_wsigaction;
	}

	// Beware! Arming the timer:
	struct itimerspec timerspec = {
		.it_value = split_time(timeout),
	};
	if (timer_settime(timer, 0, &timerspec, NULL) != 0)
		goto exit_wsigaction;

	// Run the code in question
	int ret = luabox_runv(code, memory, result);

	// Disarm the timer QUICKLY!
	timerspec.it_value.tv_sec = 0;
	timerspec.it_value.tv_nsec = 0;
	if (timer_settime(timer, 0, &timerspec, NULL) != 0) {
		// Can't disarm the timer. That may only happen if something (probably
		// the stack) is corrupted. There is no safe way to proceed. UB took it over.
		// So, halt the program as quickly as possible, to minimize potential damage.
		_exit(errno ?: -1);
	}

	// If we reached this point, we’re on the solid ground again.
	errno = ret;

exit_wsigaction:
	// This point may be reached by 2 ways: directly or after timeout.
	// But the timer is not armed here, regardless of the execution path
	if (sigaction(SIGALRM, &old_sa, NULL) != 0)
		warn("Can't reset signal handler");
exit_wtimer:
	if (timer_delete(timer) != 0)
		warn("Can't delete the timer");
exit:
	fesetenv(&fenv);
	return errno;
}

#define error(msg) ({ lua_pushliteral(L, msg); lua_error(L); })

static int lualua_run(lua_State *L)
{
	// Process arguments
	// Nothing should throw here, iff the arguments are all valid
	double time = luaL_checknumber(L, 1);
	time > 0 || error("Time limit must be positive");

	int mem = luaL_checkinteger(L, 2);
	mem > 0 || error("Memory limit must be positive");
	mem < INT_MAX / 1024 || error("Memory limit too large");
	mem *= 1024;

	int argc = lua_gettop(L) - 2;
	argc > 0 || error("At least one code argument is required");
	struct string_list *args = new_string_list(alloca, argc);
	for (int k = 0; k < argc; k++)
		args->strings[k].data = luaL_checklstring(L, k + 3, &args->strings[k].length);

	// Allocate memory
	// Userdata is used to simplify things: it will be freed by Lua afterwards,
	// even in the case something jumps out of this function.
	buffer_ref memory = { mem, lua_newuserdata(L, mem) };

	// Do it.
	struct string_list *result = NULL;
	int ret = lualua_runv(time, args, memory, &result);

	if (!result) { // NOTE: result must be set if return value is zero. Will it, actually?
		lua_settop(L, 0); // reset the stack *now*
		lua_pushboolean(L, 0);
		lua_pushstring(L, strerror(ret));
		return 2;
	}

	int addvals = ret ? 2 : 1;
	// Process result
	// `memory` can’t be freed yet as `result` is allocated inside it.
	if (!lua_checkstack(L, result->count + addvals)) {
		lua_settop(L, 0); // reset the stack *now*
		lua_pushboolean(L, 0);
		lua_pushstring(L, "Too many results");
		return 2;
	}
	int top = lua_gettop(L);
	lua_pushboolean(L, !ret);
	if (ret)
		lua_pushstring(L, strerror(ret));
	for (int k = 0; k < result->count; k++)
		lua_pushlstring(L, result->strings[k].data, result->strings[k].length);
	for (; top > 0; top--)
		lua_remove(L, 1);
	return result->count + addvals;
}

PUBLIC int luaopen_lualua(lua_State *L)
{
// 	char const *modname = luaL_checkstring(L, 1);
	lua_pushcfunction(L, lualua_run);
	return 1;
}
