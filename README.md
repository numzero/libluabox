# libluabox

*True Lua sandbox... in a Lua module!*

Features:

* Restricted Lua-5.1 build, without all the unsafe features
* Restricted memory allocator, based on dl-malloc
* Hard execution time limit using asynchronous signals
* Trivial internal API, minimizing attack surface
* Lua module external API, for easy use

Limitations:

* Linux-only
