#include <cerrno>
#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "luabox.h"

string_ref operator ""_sr(char const *data, size_t size)
{
	return { size, data };
}

string_ref code = "return \"text\", \"more!\""_sr;

bool is_same_str(string_ref a, string_ref b)
{
	if (a.length != b.length)
		return false;
	if (a.data == b.data)
		return true;
	return std::memcmp(a.data, b.data, a.length) == 0;
}

[[noreturn]] static void die(char const *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	static constexpr std::size_t m_kb = 64;
	static constexpr std::size_t m = 1024 * m_kb;
	buffer_ref mem = {
		m,
		std::malloc(m),
	};
	string_list *results;

	fprintf(stderr, "Testing with no memory\n");
	int ret = luabox_run1(code, {}, &results);
	if (ret != ENOMEM)
		die("Wrong error value, ENOMEM expected, got %s\n", strerror(ret));
	if (results)
		die("Allocation or other bug happened\n");
	fprintf(stderr, "OK, no results\n");

	fprintf(stderr, "Testing with %zu KB memory\n", m_kb);
	ret = luabox_run1(code, mem, &results);
	if (ret)
		die("Unexpected error: %s\n", strerror(ret));
	if (!results)
		die("No result returned\n");
	fprintf(stderr, "%zu results returned:\n", results->count);
	for (int k = 0; k < results->count; k++)
		fprintf(stderr, " - <%.*s>\n", (int)results->strings[k].length, results->strings[k].data);

	return EXIT_SUCCESS;
}
