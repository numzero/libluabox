### Math
Unless stated otherwise, all functinos are used in lmathlib.c
* acos
* asin
* atan
* atan2
* cos
* cosh
* exp
* floor (lcode.c)
* fmod
* frexp
* ldexp
* log
* log10
* modf
* pow (lmathlib.c, lcode.c, lvm.c)
* rand
* sin
* sinh
* sqrt
* srand
* tan
* tanh

### Conversion
* __ctype_b_loc (lobject.c, llex.c)
* strtod (lobject.c)
* strtoul (lobject.c)
* sprintf (lobject.c, lvm.c)
* localeconv (llex.c)
* strcoll (lvm.c)

### Jumping
* _setjmp (ldo.c)
* _longjmp (ldo.c)
* exit (ldo.c)

## Library safety
Remaining unsafe features are to be fixed or dropped.

lauxlib.c:
* __errno_location
* strerror
* stderr
* fprintf
* realloc
* free
* feof
* fread
* fopen
* _IO_getc
* freopen
* ungetc
* ferror
* fclose
* stdin

lbaselib.c:
* strtoul
* __ctype_b_loc
* stdout
* fputc
* fputs

lstrlib.c:
* __ctype_toupper_loc
* __ctype_tolower_loc
* __ctype_b_loc
* sprintf
