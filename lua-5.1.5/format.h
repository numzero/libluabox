#ifndef Z_FORMAT_H
#define Z_FORMAT_H

int sprintf(char *restrict s, const char *restrict fmt, ...);

#endif
