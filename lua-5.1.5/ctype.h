#ifndef Z_CTYPE_H
#define Z_CTYPE_H

static inline int islower(int c) {
	return c >= 'a' && c <= 'z';
}

static inline int isupper(int c) {
	return c >= 'A' && c <= 'Z';
}

static inline int isdigit(int c) {
	return c >= '0' && c <= '9';
}

static inline int isalpha(int c) {
	return isupper(c) || islower(c);
}

static inline int isprint(int c) {
	return c >= ' ' && c < 0x7f; // 0x7f is DEL
}

static inline int isgraph(int c) {
	return c > ' ' && c < 0x7f; // 0x7f is DEL
}

static inline int isspace(int c) {
	return c == ' ' || c == '\f' || c == '\n' || c == '\t' || c == '\v';
}

static inline int isalnum(int c)
{
	return isalpha(c) || isdigit(c);
}

static inline int iscntrl(int c) {
	return (c >= 0 && c < 0x20) || c == 0x7f;
}

static inline int ispunct(int c) {
	return isgraph(c) && !isalnum(c);
}

static inline int isxdigit(int c) {
	return (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
}

static inline int tolower(int c) {
	if (!isupper(c))
		return c;
	return c | 0x20;
}

static inline int toupper(int c) {
	if (!islower(c))
		return c;
	return c & ~0x20;
}

#endif
