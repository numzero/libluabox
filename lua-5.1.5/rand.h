#ifndef _RAND_H
#define _RAND_H
#include <stdint.h>

uint32_t nrand64(uint64_t state[static 1]) {
	state[0] ^= state[0] >> 12;
	state[0] ^= state[0] << 25;
	state[0] ^= state[0] >> 27;
	return (state[0] * 0x2545F4914F6CDD1Dull) >> 32;
}

#endif
