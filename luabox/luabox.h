/** © Copyright 2018 Lobachevskiy Vitaliy <numzer0@yandex.ru>
 *
 * This file is part of LibLuaBox.
 *
 * LibLuaBox is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibLuaBox is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR a PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with LibLuaBox.  If not, see <https://www.gnu.org/licenses/>
 *
 * @file luabox/main.c
 * @author Lobachevskiy Vitaliy <numzer0@yandex.ru>
 * @copyright GNU AGPL v3+
 */

#pragma once
#include <stddef.h>
#include <uchar.h>

#ifdef __cplusplus
#define LUABOX_PUBLIC extern "C" __attribute__((visibility ("default")))
#else
#define LUABOX_PUBLIC __attribute__((visibility ("default")))
#endif

typedef struct string_ref
{
	size_t length;
	char const *data;
} string_ref;

typedef struct buffer_ref
{
	size_t length;
	void *data;
} buffer_ref;

struct string_list
{
	size_t count;
	string_ref strings[];
};

#define new_string_list(alloc,n) ({ \
		size_t _count = (n); \
		struct string_list *_list = (struct string_list *)(alloc(sizeof(struct string_list) + _count * sizeof(string_ref))); \
		if (_list) \
			_list->count = _count; \
		_list; \
	})

/**
 * Run the restricted Lua. See @ref luabox_runv for details.
 */
LUABOX_PUBLIC int luabox_run1(string_ref code, buffer_ref memory, struct string_list **result);

/**
 * Run the restricted Lua.
 * @param[in] code Lua code to execute. All strings are effectively concatenated.
 * @param[in] memory Memory to use as the heap.
 * @param[out] result List of values returned by the chunk. Values are stored
 * at arbitrary locations in @p memory, or are statically allocated.
 * @returns Zero on success, non-zero error code on failure.
 * * ENOMEM on running out of memory.
 * * EINVAL if @p code is empty or @p result is NULL.
 * * ENOEXEC on syntax error in the code. Error message is stored in @p result.
 * * ENOTRECOVERABLE on unhandled error(). Error message is stored in @p result.
 * @warning Error codes are subject to change.
 * @note Code may not overlap with the memory given.
 * @note This function is reentrant, MT-Safe, AS-Safe and AC-Safe, provided that
 * same @p memory is not used in concurrent calls.
 */
LUABOX_PUBLIC int luabox_runv(struct string_list *code, buffer_ref memory, struct string_list **result);
