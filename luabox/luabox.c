/** © Copyright 2018 Lobachevskiy Vitaliy <numzer0@yandex.ru>
 *
 * This file is part of LibLuaBox.
 *
 * LibLuaBox is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibLuaBox is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR a PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with LibLuaBox.  If not, see <https://www.gnu.org/licenses/>
 *
 * @file luabox/main.c
 * @author Lobachevskiy Vitaliy <numzer0@yandex.ru>
 * @copyright GNU AGPL v3+
 */

#include "luabox.h"
#include <alloca.h>
#include <errno.h>
#include <string.h>
#include "malloc-2.8.6.h"
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

static void *alloc(void *ud, void *ptr, size_t osize, size_t nsize)
{
	mspace *ms = ud;
	(void) osize;
	return mspace_realloc(ms, ptr, nsize);
}

#define msalloc(size) mspace_malloc(ms, size)

// lua_CFunction
static int load_libs(lua_State *L)
{
	luaL_openlibs(L);
	return 0;
}

LUABOX_PUBLIC int luabox_runv(struct string_list *code, buffer_ref memory, struct string_list **result)
{
	if (!result)
		return EINVAL;
	if (!code->count)
		return EINVAL;
	mspace *ms = create_mspace_with_base(memory.data, memory.length, 0);
	if (!ms)
		return ENOMEM;
	lua_State *L = lua_newstate(alloc, ms);
	if (!L)
		return ENOMEM;
	if (lua_cpcall(L, load_libs, NULL) != 0)
		return ENOMEM; // There is no other way to fail here
	int err = ENOTTY;
	int ret = 0;
	for (int k = 0; k < code->count; k++) {
		int err = luaL_loadbuffer(L, code->strings[k].data, code->strings[k].length, "luabox_runv");
		switch (err) {
			case 0: break;
			case LUA_ERRSYNTAX: err = ENOEXEC; ret = 1; goto out;
			case LUA_ERRMEM: return ENOMEM;
			default: ret = 1; goto out; // NOTE: Is this ever used?
		}
		lua_insert(L, 1);
		err = lua_pcall(L, ret, LUA_MULTRET, 0);
		switch (err) {
			case 0: break;
			case LUA_ERRRUN: err = ENOTRECOVERABLE; ret = 1; goto out;
			case LUA_ERRMEM: return ENOMEM;
			default: ret = 1; goto out; // NOTE: Is this ever used?
		}
		ret = lua_gettop(L);
	}
	err = 0;
out:;
	struct string_list *results = new_string_list(msalloc, ret);
	if (!results)
		return ENOMEM;
	for (int k = 0; k < ret; k++) {
		// lua_tolstring may OOM if there is something other than string
		if (lua_isstring(L, k + 1)) {
			results->strings[k].data = lua_tolstring(L, k + 1, &results->strings[k].length);
		} else {
			results->strings[k].data = NULL;
			results->strings[k].length = 0;
		}
	}
	// no cleanup needed—everything here is dead after return
	*result = results;
	return err;
}

LUABOX_PUBLIC int luabox_run1(string_ref code, buffer_ref memory, struct string_list **result)
{
	struct string_list *list = new_string_list(alloca, 1);
	list->count = 1;
	list->strings[0] = code;
	return luabox_runv(list, memory, result);
}
